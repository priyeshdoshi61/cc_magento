<?php

namespace CC\Priyesh\Controller\Index;

use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_response;
    /**
     * Customer Repository.
     *
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * Encryptor.
     *
     * @var \Magento\Framework\Encryption\Encryptor
     */
    protected $_encryptor;

    /**
     * Customer registry.
     *
     * @var \Magento\Customer\Model\CustomerRegistry
     */
    protected $_customerRegistry;
    protected $_messageManager;

    /**
     * AccountManagementPlugin constructor.
     *
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Framework\Encryption\Encryptor           $encryptor
     * @param \Magento\Customer\Model\CustomerRegistry          $customerRegistry
     */
    public function __construct(Context $context, \Magento\Framework\App\ResponseInterface $response,
                                \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
                                \Magento\Framework\Encryption\Encryptor $encryptor,
                                \Magento\Customer\Model\CustomerRegistry $customerRegistry,
                                \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->_response = $response;
        $this->_customerRepository = $customerRepository;
        $this->_encryptor          = $encryptor;
        $this->_customerRegistry   = $customerRegistry;
        $this->_messageManager = $messageManager;
        parent::__construct($context);
    }

    public function execute()
    {
        // 1. POST request : Get booking data
        $post = (array) $this->getRequest()->getPost();

        if (!empty($post)) {
            // Retrieve your form data
            $email     = $post['email'];
            $password  = $post['password'];

            $CustomerModel = $this->_customerExists($email, $password);
            if($CustomerModel){
                $userId = $CustomerModel->getId();
                $customer = $this->_customerRepository->getById($userId);
                $this->_customerRepository->save($customer, $this->_encryptor->getHash($password, true));
                print("Password updated Successfully");
                $this->_messageManager->addSuccessMessage("Password Updated Successfully");
            } else {
                print("Email not present in Database");
                $this->_messageManager->addErrorMessage("Email not present in Database");
            }
            $this->_response->setRedirect('/Magento/priyesh/index/index')->sendResponse();
        } else {
            // 2. GET request : Render the booking page
            $this->_view->loadLayout();
            $this->_view->renderLayout();
        }
    }

    protected function _customerExists($email, $password)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
        $CustomerModel->setWebsiteId(1);
        $CustomerModel->loadByEmail($email);
        $userId = $CustomerModel->getId();
        if($userId){
            return $CustomerModel;
        } else {
            return false;
        }
    }
}